﻿using System;

namespace exercise17
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            //Greeting
            Console.WriteLine("Hi, whats up?");
            Console.ReadLine();
            
            //Question, do some maths?
            Console.WriteLine("Cool. Would you like to test your math skills? Press <y> for yes or <n> for no");
            var answer = Console.ReadLine();
            
            //If "yes" no to maths
            if (answer == "y")
           { 
            //Maths Question and read answer
            Console.WriteLine("How much is 6*81?");
            var answer2 = int.Parse(Console.ReadLine());
           
           //If correct answer
            if (answer2 == 486)
           { 
               Console.WriteLine("Well done.You've got it"); }

           //If too low answer
           if (answer2 < 486)
           { Console.WriteLine("A little more than that.. ");}

           //If too high answer
           if (answer2 > 486)
           { Console.WriteLine("A little less than that.. ");}
           }
           

           //If "no" to maths
           if (answer == "n")
           {
               Console.WriteLine("Thats ok.. Bye");
           }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
